import QtQuick 2.2
import QtQuick.Controls 2.5
import QtQuick.Window 2.15

import org.kde.kirigami 2.8 as Kirigami

Window {
    width: 320
    height: 320
    visible: true
    title: qsTr("QQC2 Label Color Animation")

    color: Kirigami.Theme.backgroundColor

    Label {
        id: label
        anchors.centerIn: parent
        text: "Color Animation Check"
        // color: Kirigami.Theme.textColor // FIXME
        Behavior on color {
            ColorAnimation {
                duration: Kirigami.Units.longDuration
            }
        }
    }
}
