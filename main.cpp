#include <QAbstractItemModel>
#include <QApplication>
#include <QGuiApplication>
#include <QRandomGenerator>
#include <QTimer>
#include <QQmlApplicationEngine>

#include <KColorSchemeManager>
#include <Kirigami/Units>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    KColorSchemeManager manager;

    QTimer timer;
    QObject::connect(&timer, &QTimer::timeout, [&manager]() {
        int index = QRandomGenerator::global()->bounded(manager.model()->rowCount());
        manager.activateScheme(manager.model()->index(index, 0));
    });
    Kirigami::Units units;
    timer.start(units.veryLongDuration());

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
